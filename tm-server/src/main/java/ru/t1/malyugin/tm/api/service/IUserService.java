package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

import java.util.List;

public interface IUserService {

    void clear();

    void removeById(
            @Nullable String id
    );

    @Nullable
    User findOneById(
            @Nullable String id
    );

    @NotNull
    List<User> findAll();

    int getSize();

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    );

    User lockUserByLogin(
            @Nullable String login
    );

    User unlockUserByLogin(
            @Nullable String login
    );

    @NotNull
    User removeByLogin(
            @Nullable String login
    );

    @NotNull
    User removeByEmail(
            @Nullable String email
    );

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateProfile(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @Nullable
    User findOneByLogin(
            @Nullable String login
    );

    @Nullable
    User findOneByEmail(
            @Nullable String email
    );

    @NotNull
    Boolean isLoginExist(
            @Nullable String login
    );

    @NotNull
    Boolean isEmailExist(
            @Nullable String email
    );

}